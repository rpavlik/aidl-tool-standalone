cmake_minimum_required(VERSION 3.4.1)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
find_package(BISON REQUIRED)
find_package(FLEX REQUIRED)

# Build this subset of Android libraries locally on its own
add_library(
  libbase-local STATIC
  libbase/abi_compatibility.cpp
  libbase/chrono_utils.cpp
  libbase/cmsg.cpp
  libbase/errors_unix.cpp
  # libbase/errors_windows.cpp
  libbase/file.cpp
  # libbase/format_benchmark.cpp
  libbase/liblog_symbols.cpp
  libbase/logging.cpp
  libbase/mapped_file.cpp
  libbase/parsebool.cpp
  libbase/parsenetaddress.cpp
  libbase/process.cpp
  libbase/properties.cpp
  libbase/stringprintf.cpp
  libbase/strings.cpp
  # libbase/test_main.cpp
  # libbase/test_utils.cpp
  libbase/threads.cpp
  # libbase/utf8.cpp
  # core/liblog/event_tag_map.cpp
  # core/liblog/logd_reader.cpp
  # core/liblog/logd_writer.cpp
  core/liblog/log_event_list.cpp
  core/liblog/log_event_write.cpp
  core/liblog/logger_name.cpp
  core/liblog/logger_read.cpp
  core/liblog/logger_write.cpp
  core/liblog/logprint.cpp
  core/liblog/log_time.cpp
  # core/liblog/pmsg_reader.cpp
  # core/liblog/pmsg_writer.cpp
  core/liblog/properties.cpp)
target_include_directories(
  libbase-local
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/libbase/include
  PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/core/liblog/include
          ${CMAKE_CURRENT_SOURCE_DIR}/core/libcutils/include
          ${CMAKE_CURRENT_SOURCE_DIR}/core/include)

flex_target(aidl_language aidl_language_l.ll
            ${CMAKE_CURRENT_BINARY_DIR}/lexer.cpp)
bison_target(
  aidl_language aidl_language_y.yy
  ${CMAKE_CURRENT_BINARY_DIR}/aidl_language_y.cpp
  DEFINES_FILE ${CMAKE_CURRENT_BINARY_DIR}/aidl_language_y.h)

add_library(
  aidl-common STATIC
  aidl.cpp
  aidl_checkapi.cpp
  aidl_const_expressions.cpp
  aidl_language.cpp
  aidl_typenames.cpp
  aidl_to_cpp.cpp
  aidl_to_java.cpp
  aidl_to_ndk.cpp
  ast_cpp.cpp
  ast_java.cpp
  code_writer.cpp
  generate_cpp.cpp
  aidl_to_cpp_common.cpp
  generate_ndk.cpp
  generate_java.cpp
  generate_java_binder.cpp
  generate_aidl_mappings.cpp
  import_resolver.cpp
  line_reader.cpp
  io_delegate.cpp
  options.cpp
  parser.cpp
  ${BISON_aidl_language_OUTPUTS}
  ${FLEX_aidl_language_OUTPUTS})
target_link_libraries(aidl-common PUBLIC libbase-local)
target_include_directories(
  aidl-common
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/libbase/include
  PRIVATE ${CMAKE_CURRENT_BINARY_DIR})

add_executable(aidl main.cpp)
target_link_libraries(aidl PRIVATE aidl-common)
