# Android AIDL Tool - Standalone

This is a fork of the upstream project, adapted to build standalone on a generic
Linux using CMake. (The original blueprint build files are left in place, but
not used.) The Android platform code that is used is provided in submodules, so
make sure to clone `--recusive` or run
`git submodule update --init --recursive`.

Minimum other dependencies are required:

- CMake - to generate a build system.
  - Plus `make`, or Ninja, or whatever build tool you wish to use.
- A C++ compiler, preferably Clang since this is what Android is built with
  - Tested to work with Clang 6.0.1 and up, though some Clang 8 features are
    used.
  - GCC 8.3.0 as found in Debian 10 "Buster" did **not** work
- Flex - tested with 2.6.4 as found in Debian 10 "Buster"
- Bison - tested with 3.3.2 as found in Debian 10 "Buster"

## Original Contents

Documentation for this project is currently maintained here:

https://source.android.com/devices/architecture/aidl/overview
